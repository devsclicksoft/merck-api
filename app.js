const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv').config({
    path: process.env.NODE_ENV === 'production' ? '.env.prod' : '.env'
});

const app = express();

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

// comentar caso deseje utilizar o banco de dados do heroku
// app.use(cors({
//     origin: process.env.BASEURL,
//     optionsSuccessStatus: 200
// }));
app.use(cors())

//IMPORTANDOS AS ROTAS DA APLICAÇÃO
app.use('/api', require('./src/app/routes/user'))
app.use('/api', require('./src/app/routes/quiz'))
app.use('/api', require('./src/app/routes/report'))

const server = app.listen(process.env.PORT || 3000, ()=>{
    console.log('Servidor rodando na porta ' + server.address().port);
    console.log('Environment: ' + process.env.NODE_ENV);
});