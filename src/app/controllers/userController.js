const userModel = require('../models/userModel');
const key = require('../config/key.json')
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * TIPOS DE USUÁRIOS
 * 1 = ADMIN
 * 2 = MANAGER / GERENTE
 * 3 = KAM
 * 4 = SALESPEOPLE / VENDEDOR
 * 
 * TOKEN (caractere add no final)
 * ADMIN = a
 * MANAGER = m
 * KAM = k
 * SALESPEOPLE = s
 */

 /**
  * ENABLE
  * 1 = USUÁRIO ATIVO
  * 0 = USUÁRIO INATIVO
  */

 /**
  * TEST AUTHORIZATION
  * 1 = AUTORIZADO REFAZER TESTE
  * 0 = DESAUTORIZADO REFAZER TESTE
  */
const utilsGenerateQuiz = require('../../utils/generateQuiz')

exports.index = async (req, res) => {
    try {
        // BUSCANDO TODAS OS USUÁRIOS CADASTRADOS E ORDENDANDO POR ORDEM ALFABÉTICA EM NAME
        const users = await userModel
            .find()
            .sort({ name: 1 })
            .where("type_user.id").gt(1)
    
        // ENVIANDO A RESPOSTA A APLICAÇÃO
        return res.status(200).json(users)
    
    } catch (err) {
        // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
        return res
            .status(400)
            .json({ error: 'Erro ao tentar buscar usuários.' })
    }
}

exports.getUser = async (req, res) => {
    try{
        // BUSCANDO UM USUÁRIO CADASTRADO PELO ID
        const user = await userModel.findById(req.params.id)

        // ENVIANDO A RESPOSTA A APLICAÇÃO
        return res.status(200).json(user)
        
    } catch (error) {
        // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
        return res.status(400)
            .json({ error: 'Erro ao tentar buscar usuário.' })
    }
}

exports.create = async(req, res) => {

    const { email, type_user } = req.body;
    
    if(!req.body.password || req.body.password === '' )
        req.body.password = req.body.email

    try {
        // VERIFICANDO SE O EMAIL ENVIADO NA REQUISIÇÃO JÁ ESTÁ CADASTRADO.
        // CASO ESTEJA CADASTRADO, RETORNA UM ERRO 400 DE USUÁRIO JÁ CADASTRADO
        if ( await userModel.findOne({ email }) ){
            return res.status(400)
                .json({ error: 'Usuário já está registrado.' });
        }

        //Necessário para uma validação em relatório
        req.body.answers = { type_quiz: null, quiz: null }
        
        // CRIANDO UM NOVO USUÁRIO COM OS DADOS DA REQUISIÇÃO
        const user = await userModel.create(req.body);

        user.password = undefined;
        
        // GERANDO UM TOKEN COM O ID DO USUÁRIO E COM A CHAVE SECRETA E UM TEMPO DE EXPIRAÇÃO.
        // ADICIONANDO LETRA AO FINAL DO TOKEN PARA CONTROLAR NÍVEIS DE PERMISSÃO
        let token = jwt.sign({ id: user.id }, key.secret, { expiresIn: 10800 })
        token += type_user.id === 2
        ? 'm' : type_user.id === 3
            ? 'k' : type_user.id === 4
                ? 's' : 'a'

        // ENVIANDO A REPOSTA A APLICAÇÃO
        res.status(200).json({ user, token });

    } catch (error) {
        return res.status(400)
            .json({ error: "Erro ao tentar registrar usuário." })
    }
}

exports.update = async (req, res) => {
    try {
        const { id } = req.params

        // BUSCANDO A USER PELO ID RECEBIDO PELA URL, E ATUALIZANDO ESTA EMPRESA COM OS DADOS NO BODY DA REQUISIÇÃO
        if( await userModel.findOneAndUpdate({ _id: id }, req.body, { new: true }) ){
            // ENVIANDO A RESPOSTA A APLICAÇÃO
            return res
                .status(201)
                .json({ success: 'Usuário atualizado com sucesso.' })
        }

        return res
            .status(400)
            .json({ error: 'Erro, usuário não encontrado.' })


    } catch (err) {
        // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
        return res
            .status(400)
            .json({ error: 'Erro ao tentar atualizar registro.' })

    }
}

exports.delete = async (req, res) => {
    try {
        const { id } = req.params

        // VALIDANDO SE O USUÁRIO EXISTE NO BANCO
        if ( !await userModel.findOne({ _id: id }) ){
            return res
                .status(400)
                .json({ error: 'ID do usuário inválido' });
        }

        // BUSCANDO UM USER PELO ID E DELETANDO
        if( await userModel.findOneAndDelete({ _id: id }) ){
            // ENVIANDO A REPOSTA A APLICAÇÃO
            return res
                .status(204)
                .json({ success: 'Usuário deletado com sucesso.' })
        }


    } catch (err) {
        // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
        return res
            .status(400)
            .json({ error: 'Erro ao tentar deletar o registro.' })
    }
}

exports.login = async(req, res) => {

    const { email, password = '', birth_date = ''} = req.body
    
    const user = await userModel.findOne({ email })
        .select(`-answers -name -email -region -users_id -created +password`)

    if (!user) return res.status(400).json({ error: 'Usuário não encontrado.' })

    // ADMIN LOGANDO
    if (password){
        
        //REFORÇANDO A SEGURANÇA
        if(user.type_user.id !== 1) 
        return res.status(400).json({ error: 'Sem permissão de acesso.' });
        
        // COMPARA A SENHA ENVIADA DA REQUISIÇÃO (MD5) COM O HASH DA SENHA
        // ARMAZENADO NO BD. CASO NÃO COINCIDAM RETORNA UM ERRO 400
        if (!await bcryptjs.compare(password, user.password))
            return res.status(400).json({ error: 'Senha incorreta.' });

        const token = jwt.sign({ id: user.id }, key.secret, { expiresIn: 10800 }) + 'a'
        return res.status(200).json({ id:user.id, token })
    }

    //USUÁRIO COMUM LOGANDO
    user.password = undefined

    if( user.test_authorization != 1 )
        return res.status(400).json({ error: 'Sinto muito, não está autorizado a refazer o teste.' });
    
    if( user.enable != 1 )
        return res.status(400).json({ error: 'Erro ao tentar logar, usuário inativo.' });

    if ( birth_date && birth_date != user.birth_date){
        return res.status(400)
            .json({ error: 'Erro ao tentar logar, dados incorretos.' })
    }

    // GERANDO UM TOKEN COM O ID DO USUÁRIO E COM A CHAVE SECRETA E UM TEMPO DE EXPIRAÇÃO.
    // ADICIONANDO LETRA AO FINAL DO TOKEN PARA CONTROLAR NÍVEIS DE PERMISSÃO
    let token = jwt.sign({ id: user.id }, key.secret, { expiresIn: 10800 })
    token += user.type_user.id === 2
    ? 'm' : user.type_user.id === 3
        ? 'k' : user.type_user.id === 4
            ? 's' : 'u' //u de undefined

    return res.status(200).json({ id:user.id, token })
}

exports.enableOrDisable = async (req, res) => {
    try{
        let msg = "ativado"
        const { id } = req.params

        const user = await userModel.findOne({ _id: id })

        if(user.enable === 0){
            user.enable = 1
        }else{
            user.enable = 0
            msg = "inativado"
        }

        await userModel.findOneAndUpdate({ _id: id }, user, { new: true })

        return res
            .status(201)
            .json({ 
                success: `Usuário ${msg} com sucesso.`,
                enable: user.enable
            })

    } catch (err) {
        return res
            .status(400)
            .json({ error: 'Erro ao tentar ativar ou desativar usuário.' })
    }
}

exports.testAuthorizationOrNot = async (req, res) => {
    try{
        let msg = "autorizado"
        const { id } = req.params

        let user = await userModel.findOne({ _id: id })

        if(user.test_authorization === 0){
            user.test_authorization = 1
            user.answers.quiz = null
            user.answers.type_quiz = null
            user.test_performed = 0
        }else{
            user.test_authorization = 0
            msg = "desautorizado"
        }

        await userModel.findOneAndUpdate({ _id: id }, user, { new: true })

        return res
            .status(201)
            .json({ 
                success: `Usuário ${msg} a refazer o teste.`,
                test_authorization: user.test_authorization
            })

    } catch (err) {
        return res
            .status(400)
            .json({ error: 'Erro ao tentar autorizar ou desautorizar usuário de refazer teste.' })
    }
}

exports.findUser = async (req, res) => {
    try{

        const user = await userModel.find({
            $and: [
                { 'type_user.id': { $gt: 1 } },

                { $or: [
                    { 'name': new RegExp(req.body.keywords, 'i') },
                    { 'email': new RegExp(req.body.keywords, 'i') },
                    { 'type_user.description': new RegExp(req.body.keywords, 'i') }
                ]}
            ]
        })

        return res.status(200).json(user)
    } catch (error) {

        return res.status(400).json({error: 'Erro ao tentar encontrar usuários.'})
    }
}

exports.findUsersReport = async (req, res) => {
    try{

        const user = await userModel.find({
            $and: [
                { 'type_user.id': { $gt: 1 } },
                { 'test_performed': 1},

                { $or: [
                    { 'name': new RegExp(req.body.keywords, 'i') },
                    { 'email': new RegExp(req.body.keywords, 'i') },
                    { 'type_user.description': new RegExp(req.body.keywords, 'i') }
                ]}
            ]
        })

        return res.status(200).json(user)
    } catch (error) {

        return res.status(400).json({error: 'Erro ao tentar encontrar usuários.'})
    }
}

exports.saveAnswers = async (req, res) => {
    try {
        const { id } = req.params

        console.log(req.body)

        await userModel.findByIdAndUpdate(id, req.body, {new: true})

        return res.status(200).json({ success: 'Respostas salvas com sucesso.' })

    } catch (error) {

        return res.status(400).json({ error: error.message })
    }
}

exports.saveAnswersTest = async (req, res) => {
    try {
        const { id } = req.params

        //IMPLEMENTAÇÃO DE TESTE
        req.body.quiz = utilsGenerateQuiz.index(req.body.type_quiz)
        //ATÉ AQUI RETIRAR

        await userModel.findByIdAndUpdate(id, {
            $set: { 'answers': req.body } 
        },
        { new: true })
        
        return res.status(200).json({ success: 'The answers has been saved' })

    } catch (error) {

        return res.status(400).json({ error: error.message })
    }
}

exports.getAllManagers = async (req, res) => {
    try{
        
        const managers = await userModel
        .find({ 'type_user.id': 2 })
        .select('_id name email')

        return res.status(200).json(managers)

    }  catch (error) {

        return res.status(400).json({error: error.message})

    }
}

exports.getAllUsersReports = async (req, res) => {
    try{
  
      const users = await userModel.find({'test_performed': 1})
  
      return res.status(200).json(users)
  
    } catch (error) {
  
      return res.status(400).json(error.message)
    }
  }