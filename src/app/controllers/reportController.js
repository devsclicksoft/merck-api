const path = require('path')

const userModel = require('../models/userModel');
const utilsCountOptionsCompetences = require('../../utils/countOptionsCompetences')
const utilsCompetencesJson = require('../../utils/json/competences.json')
const utilsPdfKit = require('../../utils/pdfKit')

exports.index = async (req, res) => {
  const { id } = req.params

  try{

    const { name, type_user, answers } = await userModel.findOne({ _id: id })

    if( !answers.type_quiz )
      return res.status(400).json({ error: "User did not answer the questionnaire." })
    
    //GERANDO OS DADOS DO RELATÓRIO
    let reports = []
    let logs = []
    // let results = []

    
    for (let i = 0; i < utilsCompetencesJson.competences.length; i++) {
      
      const competence = utilsCompetencesJson.competences[i].id
      const resultCountQuestion = utilsCountOptionsCompetences.index(competence, answers.type_quiz)
      
      if( !resultCountQuestion ){ logs = [...logs, { error: `erro ao buscar do questionário o total de opções da competência ${i+1}`}] }
      else{
        // results.push(resultCountQuestion)

        //Verificando quantas competences usuário tem no geral
        const resultCountAnswers = utilsCountOptionsCompetences.index(competence, null, answers)
        if( !resultCountAnswers ){ logs = [...logs, { error: `erro ao buscar da resposta do usuário o total de opções da competência ${i+1}`}] }
        else{
          const max = resultCountQuestion.count // 100%
          const ans = resultCountAnswers.count // ?%
          let score = 0 //100% - ?%

          // Simulação do cálculo
          // ans === 5 e max === 20
          // 5/20 = x/100 //aplica regra de 3
          // 20x = 5 * 100
          // 20x = 500
          // x = 500/20
          // x = 25 //significa que "ans" é 25% de "max"

          //Cálculo em programação
          score = ans * 100
          score = score/max

          //Aplicando regra de negócio: Cada opção marcada da competência, usuário perde score
          //Logso, na competência atual, (seguindo o exemplo) seria 100% - 25%
          //Assim obtemos com qual score ele atingiu na competência
          let loss = score
          score = 100 - score
          
          score = parseFloat(score.toFixed(0));
          loss = parseFloat(loss.toFixed(0));
          reports.push({ competence:utilsCompetencesJson.competences[i], totalOptions: max, totalMarked: ans, score, loss})
        }
      }
    }


    //GERANDO PDF DO RELATÓRIO
    /**
     * REGRA DE NEGÓCIO
     * Cada competência terá uma pontuação 0 a 100, com as seguintes classificações:
     *   - Zona Cinza: score < 20 pontos significa ausência da competência
     *   - Zona Laranja: score >= 20 ou < 40 pontos significa baixíssima competência.
     *   - Zona Amarela: score >= 40 ou < 70 pontos significa competência em desenvolvimento.
     *   - Zona Verde: score >= 70 ou < 90 pontos significa competência desenvolvida.
     *   - Zona Azul: score >= 90 pontos significa que tem a competência e é referência nela.
     */
    const resultReport = utilsPdfKit.index({
      user: {id, name, type: type_user.description },
      reports,
    })

    //ENVIANDO PDF PARA EMAIL DO GERENTE

    // return res.status(200).json({ user_id: id, type_quiz: answers.type_quiz, reports, logs })
    return res.status(200).json(resultReport)

  }catch(error){
    return res.status(400).json({ error })
  }

}


exports.download = (req, res) => {
  const { id } = req.params
  let filename = "output.pdf"
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader('Content-disposition', 'inline; filename="' + filename + '"');
  res.setHeader('Content-type', 'application/pdf')

  var file = path.resolve('public', 'pdf', `${id}.pdf`);
  res.download(file); // Set disposition and send it.
}