const utilsGetQuizFromPage = require('../../utils/getQuizFromPage')

/**
 * ESTA FUNÇÃO É PARA DISPONIBILIZAR AS QUESTÕES DE FORMA LIMITADA
 * OBS: Os questionários não estão persistentes em um banco de dados,
 * estão estáticos em arquivos json no back-end da aplicação.
 * Encontrados em /src/utils/json
 */
exports.index = async (req, res) => {
  const { page = 1, limit = 5} = req.query
  const type = 2 //Type significa o tipo do quiz que desejamos buscar.
  //No caso Quiz KAM
  
  try {
    //CASO QUEIRA ALTERAR A QUANTIDADE DE QUESTÕES RETORNADAS
    //BASTA MUDAR O ÚLTIMO PARÂMETRO DA FUNÇÃO ABAIXO
    const result = await utilsGetQuizFromPage.index(type, parseInt(page), parseInt(limit) )
    
    // ENVIANDO A RESPOSTA A APLICAÇÃO
    return res.status(200).json(result)

  } catch (err) {
      // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
      return res.status(400).json({ error: 'error fetching quiz kam.' })
  }
}