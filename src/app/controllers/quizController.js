const quizModel = require('../models/quizModel')

exports.index = async (req, res) => {
  try {
    // BUSCANDO TODAS OS USUÁRIOS CADASTRADOS E ORDENDANDO POR ORDEM ALFABÉTICA EM NAME
    const quiz = await quizModel.find()

    // ENVIANDO A RESPOSTA A APLICAÇÃO
    return res.status(200).json(quiz)

  } catch (err) {
      // CASO NÃO CONSIGA REALIZAR AS OPERAÇÕES ACIMA, RETORNA ESTE ERRO ABAIXO.
      return res
          .status(400)
          .json({ error: 'error fetching quiz.' })
  }
}

/**
 * ESTA FUNÇÃO SERVE PARA INSERIR UM QUIZ MANAGER NO BANCO DE DADOS
 */
exports.create = async (req, res) => {
    const { type_quiz } = req.body
    //const result = await quizModel.find().where('type_quiz.id').equals(type_quiz.id)
    
    if ( await quizModel.findOne({ type_quiz }) ){
      return res
        .status(400)
        .json({ error: 'Quiz already registered.', result });
    }
    
    try {
        const quiz = await quizModel.create(req.body)
        res.status(200)
          .json(quiz);

    } catch (error) {
        return res
            .status(400)
            .json({ error: "error registering quiz." })
    }
}