const jwt = require('jsonwebtoken');
const key = require('../config/key.json')

//Middleware para validação de token. As requisições só podem ser feitas
//se o middleware autenticar o token enviado pelo header da requisição

module.exports = (req, res, next)=>{
    // Armazenando os headers de autorização na constante authHeader
    const authHeader = req.headers.authorization;

    // Se não tiver nenhum token informado no header da requisição, já retorna um erro 401
    if(!authHeader)
        return res.status(401).send({ error: "Nenhum token informado!" });

    // Separando 'Bearer' de 'token' para poder validar o payload do token separadamente mais a frente
    const parts = authHeader.split(" ");

    // O token precisa ter duas partes 'splitadas'. 'Bearer' + 'Token'.
    // Caso não tenha retorna um erro 401.
    if(!parts.length === 2)
        return res.status(401).send({ error: "Tamanho do token incompatível" });
    
    // Remove o ultimo caracter do token para validação de payload.
    // Pois o ultimo caracter foi adicionado nos controllers de login dos módulos
    // para definir nivel de permissão de cada tipo de usuário ao dashboard específico
    // 'a' = 'admin', 'e' = 'empresas', 'p' = 'profissionais
    const token = parts[1].substr(0, parts[1].length - 1)

    // Verifica se o token informado sem o ultimo caracter é válido
    // Caso seja válido, ele prossegue para execução do controller.
    jwt.verify(token, key.secret, (err, decoded) =>{
        if (err) 
            return res.status(401).send({error: "Token Inválido"});

        req.userId = decoded.id;
    });

    // Permite a execução dá proximo função, no caso, controller.
    return next();
}