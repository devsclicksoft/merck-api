const express = require('express')

const routes = express.Router()

const reportController = require('../controllers/reportController')

routes.get('/report/:id', reportController.index)
routes.get('/report/download/:id', reportController.download)


module.exports = routes