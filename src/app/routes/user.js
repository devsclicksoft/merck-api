const express = require('express')

const routes = express.Router()

const userController = require("../controllers/userController")
const authMiddleware = require('../middlewares/authMiddleware')
const loginMiddleware = require('../middlewares/loginMiddleware')

/**
 * ROTA - BUSCAR TODOS OS USUÁRIOS
 * 
 * Adicionar um middleware que:
 * - Garanta que um ADMIN esteja logado para obter resposta
 */
routes.get('/users', userController.index)

/**
 *  ROTA - BUSCAR TODOS OS GERENTES
 * 
 */
routes.get('/users/manager', userController.getAllManagers)

routes.get('/user/reports',  userController.getAllUsersReports)


/**
 * ROTA - BUSCAR UM ÚNICO USUÁRIO
 * 
 * Adicionar um middleware que:
 * - Garanta que um ADMIN esteja logado para obter resposta
 * - Garanta que o ID não seja inválido
 */
routes.get('/user/:id', userController.getUser)


/**
 * ROTA - REGISTRAR UM NOVO USUÁRIO
 * 
 * Adicionar um middleware que:
 * - Garanta que um ADMIN esteja logado para obter resposta
 * - Garanta que os campos obrigatórios não estejam vazios
 * - Que a senha digitada pelo usuário esteja dentro do padrão
 */
routes.post("/user/register", userController.create)


// ATUALIZAR UM USUÁRIO
routes.put("/user/update/:id", userController.update)


// DELETAR UM USUÁRIO
routes.delete("/user/delete/:id", userController.delete)


/**
 * ROTA - EFETUAR O LOGIN DE UM USUÁRIO
 */
routes.post('/user/login', loginMiddleware, userController.login)


// ATIVAR OU INATIVAR UM USUÁRIO
routes.post('/user/enable/:id', userController.enableOrDisable)


// AUTORIZAR OU NÃO UM USUÁRIO REFAZER TESTE
routes.post('/user/testAuthorization/:id', userController.testAuthorizationOrNot)


// BUSCAR UM USUÁRIO POR NOME, EMAIL OU TIPO
routes.post('/user/find', userController.findUser)

// BUSCAR UM USUÁRIO POR NOME, EMAIL OU TIPO
routes.post('/user/find/report', userController.findUsersReport)


// SALVAR AS RESPOSTAS DO FORMULÁRIO DO USUÁRIO A CADA MUDANÇA DE PÁGINA
routes.put('/user/answers/:id', userController.saveAnswers)





//=====================================================================
//ROTAS CRIADAS PARA EXECUTAR TESTES E/OU ARQUIVOS ULTILS

/**
 * Serve para simular um usuário preenchendo os questionários
 * - Preenchimento automático
 * - Informar o type_quiz no body. Ex: { "type_quiz": 1 } // ou 2 ou 3
 */
routes.put('/user/answers/test/:id', userController.saveAnswersTest)


module.exports = routes