const express = require('express')

const routes = express.Router()

const quizController = require('../controllers/quizController')
const quizManagerController = require('../controllers/quizManagerController')
const quizKamController = require('../controllers/quizKamController')
const quizSalespeopleController = require('../controllers/quizSalespeopleController')

// routes.get('/quiz', quizController.index)
routes.get('/quiz/manager', quizManagerController.index)
routes.get('/quiz/kam', quizKamController.index)
routes.get('/quiz/salespeople', quizSalespeopleController.index)

routes.get('/quiz/gerente', quizManagerController.index)
routes.get('/quiz/kam', quizKamController.index)
routes.get('/quiz/vendedor', quizSalespeopleController.index)

module.exports = routes