const mongoose = require('../../database/index');
const bcryptjs = require("bcryptjs");

const userSchema = new mongoose.Schema({

    name: {
        type: String,
        require: true
    },
    birth_date: {
        type: String,
        require: true
    },
    region: {
        type: String,
        require: true,
    },
    type_user: {
        id: { type: Number },
        description: { type: String }
    },
    user_manager: {
        type: Object
    },
    email: {
        type: String,
        require: true,
    },
    password: {
        type: String,
        select: false,
        require: false
    },
    enable: {
        type: Number,
        require: true,
    },
    test_authorization: {
        type: Number,
        require: true,
    },
    test_performed: {
        type: Number,
        require: true,
    },
    created: {
        type: Date,
        default: Date.now,
    },
    answers: {
        type_quiz: { type: Number },
        quiz: { type: Array }
    }

}, { collection: 'users' })

userSchema.pre("save", async function(next) {
    const hash = await bcryptjs.hash(this.password, 10);
    this.password = hash;
    next();
})

const userModel = mongoose.model("userModel", userSchema);

module.exports = userModel;