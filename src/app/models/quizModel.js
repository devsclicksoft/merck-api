const mongoose = require('../../database/index');

const quizSchema = new mongoose.Schema({
/**
 * TIPANDO O QUIZ EM NÚMERO
 * 
 * 1 - Significa Quiz Manager (Questionário para Gerentes)
 * 2 - Significa Quiz KAM (Questionário para quem for KAM)
 * 3 - Significa Quiz Salespeople (Questionário para Vendedores)
 */
  type_quiz: {
    id: { type: Number },
    description: { type: String }
  },
  quiz: {
    type: Array,
    require: true,
  },
  created: {
    type: Date,
    default: Date.now,
},
    

}, { collection: 'quiz' })

const quizModel = mongoose.model("quizModel", quizSchema);

module.exports = quizModel;