const quizManagerJson = require('./json/quizManager.json')
const quizkamJson = require('./json/quizKam.json')
const quizSalespeopleJson = require('./json/quizSalespeople.json')

function aleatorio(ini, fim, next = -1) {
  /**REGRAS
   * - ini deve ser >= 0
   * - fim deve ser <= 100
   */
  let num = Math.floor(Math.random() * 10)
  let numb = Math.floor(Math.random() * 10) + ( num * Math.floor(Math.random() * 10) )
  
  while(numb === next){
    num = Math.floor(Math.random() * 10)
    numb = num * Math.floor(Math.random() * 10)
  }
  return ( numb <= fim )  && ( numb >= ini )
  ? numb : aleatorio(ini, fim, numb)
}

/**
 * O OBJETIVO É SIMULAR UM USUÁRIO RESPONDENDO A UM TIPO DE QUIZ
 * - Aceita todos os questionários implementados ou qualque outro que siga a mesma estrutura do array
 * - Retorna um array com as respostas seguindo o json do questionário
 * - Simula um perfil real de usuário, cada resposta diferente da anterior
 * 
 * PARÂMETRO TYPE
 * - Deve ser do tipo integer
 * - Não pode ser null
 */
exports.index = (type) => {
  let quizJson = {}
  
  quizJson = type === 1 
    ? quizManagerJson
    : type === 2 
      ? quizkamJson
      : type === 3 
        ? quizSalespeopleJson
        : { error: "type parameter is invalid" }

  if( quizJson.error ) return []

  let array = []
  let aux = 0
  let tam = quizJson.quiz.length

  for (let i = 0; i < tam; i++) {
    
    if( tam - i <= 2 ){
      //As ultimas duas questões diferentes (podendo selecionar + opções)
      
      let newArray = []
      let options = quizJson.quiz[i].options

      for (let y = 0; y < 10; y++) {

        let num = aleatorio(0, ( options.length - 1) )

        newArray.push({
          id: options[num].id,
          frequency: options[num].frequency,
          competence: options[num].competence
        })

        options.splice(num, 1)
        
      }

      array.push({
        id_question: quizJson.quiz[i].id,
        options: newArray
      })

    }else{

      array.push({
          id_question: quizJson.quiz[i].id,
          options: [
              {
                  id: quizJson.quiz[i].options[aux].id,
                  frequency: quizJson.quiz[i].options[aux].frequency,
                  competence: quizJson.quiz[i].options[aux].competence
              }
          ]
      })
    }

    let num = Math.floor(Math.random() * 10)
    aux = num % 2 === 0 ? 1 : 0 
  }
  return array
}