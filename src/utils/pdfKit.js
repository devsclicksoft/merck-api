const PDFKit = require('pdfkit');
const fs = require('fs');

function formatDate(num) {
  let array = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
  return array[num]
}

exports.index = (data) => {

  const pdf = new PDFKit();

  /**
   * INFOMAÇÕES IMPORTANTE PARA ENTENDIMENTO
   * - Altura máxima por página = 704
   * - Centro da página 580 / 2
   * - Cada elemento segue uma estrututa de posicionamento na página
   *   top += tamanhoDoElementoAnterior + distânciaEntreOsElementos
   */
  let widthMax = 580
  let heightMax = 704
  let height = 50
  let width = 470
  let top = 50
  let left = 70
  let text = 'Assessment de Competências'
  let path = 'public/'

  let title = {
    color: '#503291',
    fontSize: 15
  }

  let subtitle = {
    color: '#503291',
    fontSize: 12,
    marginTop: 20
  }

  let paragraph = {
    color: '#777',
    fontSize: 11,
    lineGap: 5,
    marginTop: 10
  }

  /**
   * GLOBAL
   */
  pdf.font(path + 'fonts/Roboto-Regular.ttf')

   /**
   * ====================================
   *              PAGE 1
   * ====================================
   */

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)

  /**
   * BLOCO DE DADOS DO USUÁRIO
   */
  // Tamanho bloco cabeçalho = 50
  // Distância entre os blocos = 20
  top += 70 

  // Texto do tipo de usuário
  text = data.user.type
  text = text.toUpperCase()
  pdf.fontSize(11)
    .fillColor('#bbb')
    .text(text, left, top)
  
  // Texto com nome do usuário
  text = data.user.name
  text = text.toUpperCase()
  pdf.fontSize(15)
    .fillColor(paragraph.color)
    .text(text, left, top + 20)

  // Texto com a data que usuário realizou questionário
  // text = 'Teste concluído em 17 abr 2020'
  // pdf.fontSize(10)
  //   .fillColor('#bbb')
  //   .text(text, left + 325, top + 26)
  
  /**
   * BLOCO DE INTRODUÇÃO
   */
  // Tamanho bloco cabeçalho = 40
  // Distância entre os blocos = 30
  top += 70 

  // Título
  text = 'INTRODUÇÃO'
  pdf.fontSize(title.fontSize)
    .fillColor(title.color)
    .text(text, left, top)

  // Parágrafos 1
  top += 20 + paragraph.marginTop
  text = 'O Assessment de Competências é uma metodologia que identifica oportunidades em 6 competências técnicos funcionais estabelecidas pela Merck. Para quem exerce a função de liderança com gerenciamento de pessoas é considerada mais uma competência específica de Gestão de Pessoas.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 2
  top += 75 + paragraph.marginTop
  text = 'Será apresentado neste relatório, o perfil no que diz respeito a estas competências, de acordo com as respostas dadas no questionário online por você. Serão apresentados resultados numéricos (escores) de cada uma das competências e as interpretações destes resultados.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 3
  top += 58 + paragraph.marginTop
  text = 'A partir da leitura deste relatório será possível uma conversa estruturada e de mais qualidade sobre seus pontos fortes e de desenvolvimento no que se refere aos seus conhecimentos e comportamentos que fortalecem cada uma das competências.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Subtítulos
  top += 55 + subtitle.marginTop
  text = 'Mas, o que é Competência?'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 4
  top += 15 + paragraph.marginTop
  text = 'Competência é o conjunto de conhecimentos, habilidades e atitudes que, expressadas de forma frequente e observável, contribuem para a realização de uma atividade e agregam valor nos resultados esperados, aumentando sua performance e reconhecimento pessoal.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 5
  top += 55 + paragraph.marginTop
  text = 'Recomendamos que a tais observações e reflexão gerem oportunidades para planejar ações efetivas para que você possa assumir seu protagonismo no autodesenvolvimento e gestão da sua carreira.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 6
  top += 55 + paragraph.marginTop
  text = 'Boa leitura!'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})
  
  // Numeração da página
  top += heightMax - top
  text = '1'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})

  /**
   * ====================================
   *              PAGE 2
   * ====================================
   */
  top = 50
  text = 'Assessment de Competências'
  pdf.addPage()

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)

/**
   * BLOCO DE COMPETÊNCIAS AVALIADAS
   */
  // Tamanho bloco cabeçalho = 40
  // Distância entre os blocos = 30
  top += 85 

  // Título
  text = 'COMPETÊNCIAS AVALIADAS'
  pdf.fontSize(title.fontSize)
    .fillColor(title.color)
    .text(text, left, top)

  // Subtítulos
  top += 10 + (15 + subtitle.marginTop)
  text = 'Visão Estratégica do Negócio'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 1
  top += 15 + paragraph.marginTop
  text = 'Identificar metas de longo prazo e promover a implementação de soluções, ideias e alternativas para o seu negócio e do cliente. Capacidade de se reinventar a partir da visão estratégica dos riscos e impactos no resultado do negócio, criando novas maneiras de agir frente às tarefas e aos desafios. Toma decisões baseadas em informações do negócio, envolvendo conhecimento da conta, ferramentas de análise disponibilizadas (Roambi, Pharmalink), e outras informações para um direcionamento estratégico no cliente.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Subtítulos
  top += 105 + subtitle.marginTop
  text = 'Planejamento'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 2
  top += 15 + paragraph.marginTop
  text = 'Desenvolver planos de contingência como prevenção a possíveis obstáculos, considerando informações de mercado, concorrência e debilidades dos serviços. É eficiente na administração das contas, exigências múltiplas e prazos conflitantes, monitoramento de ações e campanhas e gestão de inventários envolvendo as áreas chave para garantia de um planejamento de 360 graus. Possuir conhecimento do produto e/ou serviço que está sendo promovido e usar essas informações a favor da negociação estabelecendo metas e objetivos apropriados para a gestão do seu negócio.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Subtítulos
  top += 122 + subtitle.marginTop
  text = 'Comunicação'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 3
  top += 15 + paragraph.marginTop
  text = 'Promove clareza no processo de troca de informações bem como adapta a postura e comportamento de acordo com cada cliente. Compartilhar prontamente informações e conhecimentos úteis com a equipe e com o cliente. Ajustar mensagens, vocabulário e forma de escrita para se comunicar de forma assertiva com os clientes internos e externos.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})
  
  // Numeração da página
  top += heightMax - top
  text = '2'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})


/**
   * ====================================
   *              PAGE 3
   * ====================================
   */
  top = 50
  text = 'Assessment de Competências'
  pdf.addPage()

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)

/**
   * BLOCO DE COMPETÊNCIAS AVALIADAS
   */
  // Tamanho bloco cabeçalho = 40
  // Distância entre os blocos = 30
  top += 55 

  // Subtítulos
  top += 10 + subtitle.marginTop
  text = 'Negociação'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 4
  top += 15 + paragraph.marginTop
  text = 'Buscar acordos onde ambas as partes prosperem, compreendam suas necessidades e equilibrem as vantagens e benefícios que serão distribuídos entre as partes. Adaptar e ajustar as condições comerciais e tomada de decisões às circunstâncias novas e mutáveis do cliente ou da empresa. Recuperar-se de desapontamentos, rejeição, expectativas não cumpridas e outros imprevistos e influenciar efetivamente o cliente durante a negociação.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Subtítulos
  top += 90 + subtitle.marginTop
  text = 'Gestão dos Stakeholders'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 1
  top += 15 + paragraph.marginTop
  text = 'Identificar todos os envolvidos que, de alguma forma, podem influir no sucesso do negócio. Avalia quais informações coletadas serão estrategicamente utilizadas para os interesses e os objetivos dos envolvidos, analisando riscos e necessidades do negócio. Avalia e decide como utilizar de forma estratégica seus serviços mediante graus de poder, influência e interesses de todos os envolvidos.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Subtítulos
  top += 90 + subtitle.marginTop
  text = 'Foco no Cliente'
  pdf.fontSize(subtitle.fontSize)
    .fillColor(subtitle.color)
    .text(text, left, top)

  // Parágrafos 2
  top += 15 + paragraph.marginTop
  text = 'Coletar informações sobre os negócios e perfil do cliente para entender as expectativas dele em relação aos resultados e prever a solução de possíveis problemas. Ouve ativamente, avalia as necessidades, sugestões e feedbacks do cliente e se antecipa em estratégias e entregas que atendam as suas necessidades e superem as suas expectativas. Atua como facilitador envolvendo diversas áreas de ambas as organizações, garantindo agilidade e precisão nos resultados.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})
  
  // Numeração da página
  top += heightMax - top
  text = '3'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})


/**
   * ====================================
   *              PAGE 4
   * ====================================
   */
  top = 50
  text = 'Assessment de Competências'
  pdf.addPage()

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)

  /**
   * BLOCO DE INTERPRETANDO OS RESULTADOS
   */
  // Tamanho bloco cabeçalho = 40
  // Distância entre os blocos = 30
  top += 85 

  // Título
  text = 'INTERPRETANDO OS RESULTADOS'
  pdf.fontSize(title.fontSize)
    .fillColor(title.color)
    .text(text, left, top)

  // Parágrafos 1
  top += 15 + (15 + paragraph.marginTop)
  text = 'Cada competência terá uma pontuação 0 a 100, com as seguintes classificações:'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 2
  top += 0 + paragraph.marginTop
  text = `
  • Zona Cinza: abaixo de 20 pontos significa ausência da competência
  • Zona Laranja: de 20 a 39 pontos significa baixíssima competência.
  • Zona Amarela: de 40 a 69 pontos significa competência em desenvolvimento.
  • Zona Verde: de 70 a 89 pontos significa competência desenvolvida.
  • Zona Azul: acima de 90 pontos significa que tem a competência e é referência nela.`
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left+30, top, {align: 'justify', lineGap: paragraph.lineGap})

  // Parágrafos 3
  top += 110 + paragraph.marginTop
  text = 'Consideradas as variáveis contextuais como tempo na função e/ou novas atividades e desafios, estes resultados poderão apontar potencialidades ou necessidades de desenvolvimento. Além destes números, as competências terão associadas a elas comportamentos observáveis que evidenciam a capacidade do indivíduo em ter ou não repertórios de conhecimentos e habilidades práticas, o que irá ajudar muito na reflexão e na construção dos planos de desenvolvimento. O objetivo é representar como o indivíduo manifesta em cada uma das competências avaliadas.'
  pdf.fontSize(paragraph.fontSize)
  .fillColor(paragraph.color)
  .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})
  
  // Numeração da página
  top += heightMax - top
  text = '4'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})


/**
   * ====================================
   *              PAGE 5
   * ====================================
   */
  top = 50
  text = 'Assessment de Competências'
  pdf.addPage()

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)

  /**
   * BLOCO DE AVALIAÇÃO GERAL
   */
  top += 85 

  // Título
  text = 'AVALIAÇÃO GERAL'
  pdf.fontSize(title.fontSize)
    .fillColor(title.color)
    .text(text, left, top)


  let arrayZonas = [
    {zona:'cinza', description: 'Ausência'},
    {zona:'laranja', description: 'Bem baixa'},
    {zona:'amarela', description: 'Em desenvolvimento'},
    {zona:'verde', description: 'Bem desenvolvida'},
    {zona:'azul', description: 'É referência'}
  ]

  for (let i = 0; i < data.reports.length; i++) {
    if( data.reports[i].score ) {

      let score = data.reports[i].score

      //Verificando zona do usuário
      let pos = score < 20
      ? 0 : score >= 20 && score < 40
      ? 1 : score >= 40 && score < 70
      ? 2 : score >= 70 && score < 90
      ? 3 : score > 90 ? 4 : 0
      
      //Posicionando resultados em tela
      if(i % 2 === 0){
        //Exibir a direita
  
        if(i === 0) top += 55
        else top+= 100
        
        //Coluna 1
        text = data.reports[i].competence.description
        pdf.fontSize(11).fillColor('#000').text(text, left, top)
        
        text = '' + score
        pdf.fontSize(20).fillColor('#000').text(text, left+25, top + 23)

        pdf.image(path + 'images/barra-cinza.jpg', left, top + 50, {width: 80, height: 5})
        pdf.image(path + `images/zona-${arrayZonas[pos].zona}.jpg`, left, top + 50, {width:(score*80)/100, height: 5})
        pdf.fontSize(8).fillColor('#999').text(arrayZonas[pos].description, left, top + 60)
        
        // pdf.fontSize(10).fillColor('#999').text('21 opções ao todo', left + 110, top + 15)
        // pdf.fontSize(10).fillColor('#999').text('6 Opções marcadas', left + 110, top + 35)
        // text = data.reports[i].loss + ' pontos de perda'
        // pdf.fontSize(10).fillColor('#999').text(text, left + 110, top + 35)
        
      }else{
  
        //Coluna 2
        text = data.reports[i].competence.description
        pdf.fontSize(11).fillColor('#000').text(text, left + 250, top)
        text = '' + score
        pdf.fontSize(20).fillColor('#000').text(text, left+275, top + 23)
        pdf.image(path + 'images/barra-cinza.jpg', left+250, top + 50, {width: 80, height: 5})
        pdf.image(path + `images/zona-${arrayZonas[pos].zona}.jpg`, left+250, top + 50, {width:(score*80)/100, height: 5})
        pdf.fontSize(8).fillColor('#999').text(arrayZonas[pos].description, left+250, top + 60)
  
        // pdf.fontSize(10).fillColor('#999').text('17 opções ao todo', left + 360, top + 15)
        // pdf.fontSize(10).fillColor('#999').text('1 Opções marcadas', left + 360, top + 35)
        // text = data.reports[i].loss + ' pontos de perda'
        // pdf.fontSize(10).fillColor('#999').text(text, left + 360, top + 35)
  
      }
    }
    
  }

  // Numeração da página
  top += heightMax - top
  text = '5'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})
  

  /**
   * ====================================
   *              PAGE 6
   * ====================================
   */
  top = 50
  text = 'Assessment de Competências'
  pdf.addPage()

  /**
   * BLOCO DE CABEÇALHO
   */

  // Imagem de background
  pdf.image(
    path + 'images/background.jpg',
    left,
    top,
    { width, height }
  )

  // Logo da Merck
  pdf.image(
    path + 'images/logo.png',
    left + 25,
    top + 20,
    { width: 75, height: 12 }
  )
  
  // Texto referente ao arquivo
  pdf.fontSize(14)
      .fillColor('#f5f5f5')
      .text(text, left + 255, top + 10)

  // Texto com a data de geração do relatório
  text = new Date().getDate()
  text += ' ' + formatDate( new Date().getMonth() )
  text += ' ' + new Date().getFullYear()
  pdf.fontSize(10)
    .fillColor('#f5f5f5')
    .text(text, left + 388, top + 30)
  
    /**
   * BLOCO DE AVALIAÇÃO DETALHADA POR COMPETÊNCIA
   */
  // Tamanho bloco cabeçalho = 40
  // Distância entre os blocos = 30

  top += 85 

  // Título
  text = 'AVALIAÇÃO DETALHADA POR COMPETÊNCIA'
  pdf.fontSize(title.fontSize)
    .fillColor(title.color)
    .text(text, left, top)


  // Parágrafos 1
  top += 15 + (5 + paragraph.marginTop)
  text = 'Abaixo você irá identificar os comportamentos observáveis que evidenciaram sua pontuação em cada uma das competências.'
  pdf.fontSize(paragraph.fontSize)
    .fillColor(paragraph.color)
    .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})

  //Vendo as 3 competências com melhores scores
  let arrayBetterSkills = []
  let arrayAux = data.reports
  for (let i = 0; i < 3; i++) {

    let indexRemove = 0
    let maiorNum = 0
    for (let y = 0; y < arrayAux.length; y++){
      if(arrayAux[y].score > maiorNum){
        indexRemove = y
        maiorNum = arrayAux[y].score
      }
    }
    
    arrayBetterSkills.push(arrayAux[indexRemove])
    arrayAux.splice(indexRemove, 1)
    
  }

  for (let i = 0; i < arrayBetterSkills.length; i++) {
    let score = arrayBetterSkills[i].score
    let pos = score >= 65
    ? 0 : score > 39 && score < 65
      ? 1 : score <= 39
        ? 2 : 0

    let aux = score < 20
    ? 0 : score >= 20 && score < 40
    ? 1 : score >= 40 && score < 70
    ? 2 : score >= 70 && score < 90
    ? 3 : score > 90 ? 4 : 0

    if(i === 0) top += 65
    else top += 100
    pdf.image(path + `images/zona-${arrayZonas[aux].zona}.jpg`, left, top, {width:80, height: 25})
    text = score + ' pontos'
    pdf.fontSize(subtitle.fontSize)
    .fillColor("#fdfdfd")
    .font(path + 'fonts/Roboto-Bold.ttf')
    .text(text, left+14, top+5, {align: 'justify', lineGap: paragraph.lineGap})

    // Subtítulos
    top += 10 + subtitle.marginTop
    text = arrayBetterSkills[i].competence.description
    pdf.fontSize(subtitle.fontSize)
      .fillColor(subtitle.color)
      .font(path + 'fonts/Roboto-Regular.ttf')
      .text(text, left, top)

    // Parágrafos 2
    top += 15 + paragraph.marginTop
    text = arrayBetterSkills[i].competence.zonas[pos].description
    pdf.fontSize(paragraph.fontSize)
    .fillColor(paragraph.color)
    .text(text, left, top, {align: 'justify', lineGap: paragraph.lineGap})
  }

  // Numeração da página
  top += heightMax - top
  text = '6'
  pdf.fontSize(subtitle.fontSize)
  .fillColor(subtitle.color)
  .text(text, (widthMax / 2), top, {align: 'justify', lineGap: paragraph.lineGap})
  
  
  pdf.pipe(fs.createWriteStream(`public/pdf/${data.user.id}.pdf`));
  pdf.end();
  return { success: 1, message: "Relatório gerado"}
}