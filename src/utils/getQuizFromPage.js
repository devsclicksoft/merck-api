const quizManagerJson = require('./json/quizManager.json')
const quizkamJson = require('./json/quizKam.json')
const quizSalespeopleJson = require('./json/quizSalespeople.json')

/**
 * ESTA FUNÇÃO SERVE PARA RETORNAR OS DADOS DO QUIZ DESEJADO
 * 
 * OBS1: Nenhum parâmetro pode ser NULL
 * OBS2: Todos os parâmetros devem ser obrigatóriamente do tipo INTEGER
 */
exports.index = (type, page, limit) => {

  let quizJson = {}

  quizJson = type === 1 
    ? quizManagerJson
    : type === 2 
      ? quizkamJson
      : type === 3 
        ? quizSalespeopleJson
        : { error: "type parameter is invalid" }
  
  if( quizJson.error ) return quizJson
  if( !page ) return { error: "page parameter is invalid" }
  if( !limit ) return { error: "limit parameter is invalid" }

  const { type_quiz, quiz } = quizJson
  
  let start = (parseInt(page) - 1) * limit
  let newQuiz = []
  let traveled = 0
  let missing = start + limit

  //Essa validação serve apenas para o formato atual de questões
  if( ( page * limit ) === quiz.length + 3){
    newQuiz.push(quiz[quiz.length-2])
    missing = quiz.length-1
    traveled = 1
  }
  else if( ( page * limit ) > quiz.length + 3){
    newQuiz.push(quiz[quiz.length-1])
    missing = quiz.length
    traveled = 0
  }
  else {
    for (let i = start; i < ( start + limit ); i++) newQuiz.push(quiz[i])

    traveled = ( quiz.length - ( start + limit ) ) <= 0
      ? quiz.length
      : ( quiz.length - ( start + limit ) )
  }

  // ENVIANDO A RESPOSTA A APLICAÇÃO
  return {
        header: {
          page: parseInt(page),
          countQuiz: newQuiz.length,
          limit,
          missing,
          traveled,
          countTotal: quiz.length,
        },
        type_quiz,
        quiz: newQuiz
      }
}