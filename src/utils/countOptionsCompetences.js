const quizManagerJson = require('./json/quizManager.json')
const quizkamJson = require('./json/quizKam.json')
const quizSalespeopleJson = require('./json/quizSalespeople.json')
/**
 * O OBJETIVO É CONTAR O TOTAL DE COMPETENCIAS DE UM ARRAY
 * 
 * PARÂMETROS
 * 1º: competence => não pode ser null e deve ser um inteiro
 * 2º: type => pode ser null, se não for, deve ser um inteiro > 0 e < 4
 * 3º: answers => pode ser null, se não for, deve ser um objeto contem um array chamado quiz
 * 
 * OBS1: o type e o answers não podem ser null juntos, um dos 2 tem que ser passado
 * OBS2: o competence é um parâmetro obrigatório
 * OBS3: o answers deve seguir uma estrutura json já definida (veja em /src/utils/json/structureAnswers.json),
 *       caso contrário a função não executará corretamente
 * 
 * Essa função foi desenvolvida para atender a 2 casos:
 * 
 * 1º: Passando a competence e o type => retorna um json com a competence e o count total de opções 
 *     que contém esta competência dos questionários prontos em json (veja em /src/utils/json/)
 * 
 * 2º: Passando a competence, o type null, e o answers => retorna um json também, com a competencia
 *     porém o count agora é o total em que esta competência aparace na resposta do questionário (answers)
 */
exports.index = (competence, type = null, answers = null) => {
  
  if(!type && !answers) return null

  let quizJson = {}

  if( type ){
    quizJson = type === 1 
      ? quizManagerJson
      : type === 2 
        ? quizkamJson
        : type === 3 
          ? quizSalespeopleJson
          : null
  }

  if( answers ) quizJson = answers
  
  if( !quizJson || !competence) return null

  let tam = quizJson.quiz.length
  let countOptionsCompetences = 0
  
  //Futuramente, pensar numa forma melhor, para tirar o alto custo computacional
  //Talvés tentar recursividade
  for (let i = 0; i < tam; i++){
    for (let y = 0; y < quizJson.quiz[i].options.length; y++){
      for (let z = 0; z < quizJson.quiz[i].options[y].competence.length; z++){
        if(quizJson.quiz[i].options[y].competence[z].id === competence)
          countOptionsCompetences++
      }
    }
  }

  return { competence, count: countOptionsCompetences }
}